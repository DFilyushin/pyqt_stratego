# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QPixmap


class AboutDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(AboutDialog, self).__init__(parent)

        self.buttonBox = QtGui.QDialogButtonBox(self)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok)

        self.aboutLabelIcon = QtGui.QLabel(self)
        self.aboutLabelIcon.setPixmap(QPixmap('images//Strategy-Icon.png'))

        self.aboutLabel = QtGui.QLabel(self)
        self.aboutLabel.setText("PyQt Stratego Game\nFilyushin D., 2016")

        self.verticalLayout = QtGui.QVBoxLayout(self)
        self.verticalLayout.addWidget(self.aboutLabelIcon)
        self.verticalLayout.addWidget(self.aboutLabel)
        self.verticalLayout.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.accept)


