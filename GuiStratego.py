# -*- coding: utf-8 -*-
import sys
from PyQt4 import QtCore
from PyQt4.QtGui import QIcon, QAction
from PyQt4.QtGui import QMainWindow, QApplication, QWidget, QGridLayout, QPixmap
from Stratego import Stratego, Coord, Figure, EmptyField, Lake
from FigureLabel import FigureLabel
from About import AboutDialog
from SelectGame import SelectGameDialog
from CreateGame import CreateGameDialog
import urllib
import requests
import json


class StrategoWindow(QMainWindow):

    def __init__(self, game):
        super(StrategoWindow, self).__init__()
        self._game_id = None
        self._client_id = None
        self._client_color = None
        self._server = None
        self._timer = QtCore.QTimer()
        self.selectedField = None
        self.mainLayout = None
        self.menu = None
        self.file_menu = None
        self.about_menu = None
        self.grid = None
        self._game = game
        self._is_ready = None
        print('%s is first gamer' % game.who_next)
        self.init_ui()

    def init_ui(self):
        self.mainLayout = QWidget()
        self.grid = QGridLayout()
        self.menu = self.menuBar()
        self.mainLayout.setLayout(self.grid)
        self.setCentralWidget(self.mainLayout)
        self.setGeometry(300, 300, 400, 300)

        # set center screen
        g = self.geometry()
        g.moveCenter(QApplication.desktop().availableGeometry().center())
        self.setGeometry(g)

        self.statusBar().showMessage('Ready')
        self.setWindowIcon(QIcon("images/icon.png"))
        self.setWindowTitle('PyQt Stratego Game')

        exit_action = QAction(QIcon('images//exit.png'), '&Exit', self)
        exit_action.setShortcut('Ctrl+Q')
        exit_action.setStatusTip('Exit application')
        exit_action.triggered.connect(QApplication.quit)

        self.game_action = QAction(QIcon('images//new.png'), 'New &game', self)
        self.game_action.setStatusTip('New game')
        self.game_action.triggered.connect(self.new_game)

        self.new_game_action = QAction(QIcon('images//server.png'), 'New server &game', self)
        self.new_game_action.setShortcut('Ctrl+N')
        self.new_game_action.setStatusTip('New server game')
        self.new_game_action.triggered.connect(self.create_new_game)

        self.game_connect = QAction(QIcon('images//connect.png'), 'Connect', self)
        self.game_connect.setStatusTip('Connect to server')
        self.game_connect.triggered.connect(self.connect_to_game)

        self.game_cancel = QAction(QIcon('images//cancel.png'), 'End game', self)
        self.game_cancel.setStatusTip('End game')
        self.game_cancel.triggered.connect(self.cancel_game)


        about_action = QAction(QIcon('images//info.png'), '&About', self)
        about_action.setStatusTip('About game')
        about_action.triggered.connect(self.show_about)

        self.file_menu = self.menu.addMenu('&File')
        self.about_menu = self.menu.addMenu('&Help')
        self.file_menu.addAction(self.game_action)
        self.file_menu.addAction(self.new_game_action)
        self.file_menu.addAction(self.game_connect)
        self.file_menu.addAction(self.game_cancel)
        self.file_menu.addSeparator()
        self.file_menu.addAction(exit_action)
        self.about_menu.addAction(about_action)

        self.create_game_field()

        self.show()

    def cancel_game(self):
        """
        Cancel actual game without winner
        :return:
        """
        self._game.end_game()
        self._game_id = None
        self._client_color = None
        self._client_id = None
        self.new_game_action.setEnabled(True)
        self.game_connect.setEnabled(True)

    def show_about(self):
        # show about dialog
        dialog = AboutDialog(self)
        dialog.show()

    def connect_to_game(self):
        server, game_name, game_id, ok = SelectGameDialog.get_game()
        if ok:
            query_str = '%s/connect/%s' % (server, game_id)
            response = urllib.urlopen(query_str)
            data = json.loads(response.read())
            if data['status'] == 'Ok':
                self._server = server
                self._game_id = game_id
                self._client_id = data['client_id']
                self._client_color = data['color']
                self.new_game_action.setEnabled(False)
                self.game_connect.setEnabled(False)
                self.statusBar().showMessage('Connected to game with id %s' % self._game_id)
                self.load_field()

    def create_new_game(self):
        """
        Create new game
        :return:
        """
        server, name, color, ok = CreateGameDialog.create_game()
        if ok:
            query_str = '%s/new/%s?name=%s' % (server, color, name)
            response = urllib.urlopen(query_str)
            print(response.code)
            data = json.loads(response.read())
            if data['status'] == 'Ok':
                self._server = server
                self._game_id = data['game_id']
                self._client_id = data['client_id']
                self._client_color = data['color']
                self.new_game_action.setEnabled(False)
                self.game_connect.setEnabled(False)
                self.statusBar().showMessage('Game created with id %s' % self._game_id)

    def new_game(self):
        # start new game
        self._game.new_game()
        self.clear_grid()
        self.set_game_field()
        self.statusBar().showMessage(self._game.who_next)

    def load_field(self):
        """
        Load game field for current color
        :return:
        """
        query_str = '%s/game/%s/%s' % (self._server, self._game_id, self._client_id)
        response = urllib.urlopen(query_str)
        data = json.loads(response.read())
        for item in data['field']:
            x = item['x']
            y = item['y']
            f = item['figure']
            label = self.grid.itemAtPosition(x, y).widget()
            # coord = Coord(x, y)
            if f == 'Lak':
                label.setPixmap(QPixmap("images//water.png"))
            elif f == '?':
                label.setPixmap(QPixmap("images//shield.png"))
            elif f == '   ':
                label.setPixmap(QPixmap())
            else:
                image = "images//%s_%s.png" % (self._client_color, str(f))
                label.setPixmap(QPixmap(image))

    def field_clicked(self, source):
        if self._game.game_over:
            return
        moving = False
        target_coord = Coord(source.data()[0], source.data()[1])
        target_figure = self._game.field.get_position(target_coord)
        have_selected = self.has_selected()
        selected_position = self.get_selected()

        # can select only figure
        if isinstance(target_figure, Figure):
            if source.selected():
                source.setSelected(False)
            else:
                if have_selected:
                    coord_data = self.get_selected()
                    figure_coord = Coord(coord_data[0], coord_data[1])
                    moving = self._game.move(figure_coord, target_coord)
                    old_position = coord_data
                    new_position = source.data()
                    self.clear_selected()
                else:
                    source.setSelected(True)
        elif isinstance(target_figure, EmptyField):
            # Target position is empty field
            if have_selected:
                figure_coord = Coord(selected_position[0], selected_position[1])
                self.clear_selected()
                moving = self._game.move(figure_coord, target_coord)
                old_position = source.data()
                new_position = selected_position
        if moving:
            self.repaint_field(old_position)
            self.repaint_field(new_position)
        if self._game.game_over:
            message = 'Game over. Winner is %s' % self._game.winner
        else:
            message = '%s is next player' % self._game.who_next
        self.statusBar().showMessage(message)

    def eventFilter(self, source, event):
        if event.type() == QtCore.QEvent.MouseButtonPress:
            if isinstance(source, FigureLabel):
                self.field_clicked(source)

        return super(StrategoWindow, self).eventFilter(source, event)

    def get_selected(self):
        # get coord selected item
        items = (self.grid.itemAt(i) for i in range(self.grid.count()))
        for w in items:
            if isinstance(w.widget(), FigureLabel):
                if w.widget().selected():
                    return w.widget().data()

    def has_selected(self):
        # has selected item?
        items = (self.grid.itemAt(i) for i in range(self.grid.count()))
        for w in items:
            if isinstance(w.widget(), FigureLabel):
                if w.widget().selected():
                    return True

    def clear_selected(self):
        # clear all selected
        items = (self.grid.itemAt(i) for i in range(self.grid.count()))
        for w in items:
            if w.widget().selected():
                w.widget().setSelected(False)
                w.widget().setStyleSheet('')
                return

    def get_widget_by_position(self, position):
        # get item by coordinate
        items = (self.grid.itemAt(i) for i in range(self.grid.count()))
        for w in items:
            if w.data()[0] == position[0] and w.data()[1] == position[1]:
                return w

    def create_game_field(self):
        positions = [(i, j) for i in range(10) for j in range(10)]
        for position in positions:
            label = FigureLabel()
            label.installEventFilter(self)
            label.setAlignment(QtCore.Qt.AlignCenter | QtCore.Qt.AlignVCenter)
            label.setData(position)
            self.grid.addWidget(label, *position)

    def set_game_field(self):
        positions = [(i, j) for i in range(10) for j in range(10)]
        for position in positions:
            label = self.grid.itemAtPosition(position[0], position[1]).widget()
            coord = Coord(position[0], position[1])
            if isinstance(self._game.field.get_position(coord), Figure):
                figure = self._game.field.get_position(coord)
                png_warrior = "images//%s_%s.png" % (figure.color, figure.rank)
                """
                if figure.color == 'blue':
                    png_warrior = "images//%s_%s.png" % (figure.color, figure.rank)
                else:
                    png_warrior = "images//shield.png"
                """
                label.setPixmap(QPixmap(png_warrior))
            elif isinstance(self._game.field.get_position(coord), Lake):
                label.setPixmap(QPixmap("images//water.png"))

    def clear_grid(self):
        items = (self.grid.itemAt(i) for i in range(self.grid.count()))
        for w in items:
            w.widget().clear()

    def show_field(self):
        positions = [(i, j) for i in range(10) for j in range(10)]
        for position in positions:
            label = FigureLabel()
            label.installEventFilter(self)
            label.setAlignment(QtCore.Qt.AlignCenter | QtCore.Qt.AlignVCenter)
            coord = Coord(position[0], position[1])
            if isinstance(self._game.field.get_position(coord), Figure):
                figure = self._game.field.get_position(coord)
                png_warrior = "images//%s_%s.png" % (figure.color, figure.rank)
                """
                if figure.color == 'blue':
                    png_warrior = "images//%s_%s.png" % (figure.color, figure.rank)
                else:
                    png_warrior = "images//shield.png"
                """
                label.setPixmap(QPixmap(png_warrior))
            elif isinstance(self._game.field.get_position(coord), Lake):
                label.setPixmap(QPixmap("images//water.png"))
            label.setData(position)

            self.grid.addWidget(label, *position)

    def repaint_field(self, coord):
        x, y = coord
        items = (self.grid.itemAt(i) for i in range(self.grid.count()))
        for w in items:
            if w.widget().data()[0] == x and w.widget().data()[1] == y:
                address = Coord(x, y)
                figure = self._game.field.get_position(address)
                if isinstance(figure, Figure):
                    png_warrior = "images//%s_%s.png" % (figure.color, figure.rank)
                    """
                    if figure.color == 'blue':
                        png_warrior = "images//%s_%s.png" % (figure.color, figure.rank)
                    else:
                        png_warrior = "images//shield.png"
                    """
                    w.widget().setPixmap(QPixmap(png_warrior))
                    break
                elif isinstance(figure, EmptyField):
                    w.widget().setPixmap(QPixmap())
                    break

    def move_figure(self, old, new):
        move_data = {}
        move_data['game_id'] = self._game_id
        move_data['client_id'] = self._client_id
        move_data['x1'] = old.x
        move_data['y1'] = old.y
        move_data['x2'] = new.x
        move_data['y2'] = new.y
        response = requests.post('%s/move', data=move_data)
        data = json.loads(response.read())
        print data

    def wait_opponent(self):
        query_str = '%s/game/%s' % (self._server, self._game_id)
        response = urllib.urlopen(query_str)
        if response.code == 201:
            data = json.loads(response.read())
            if data['status'] == 'Ok':
                if data['is_full']:
                    self.statusBar().showMessage('Two players ready to game!')
                    self.load_field()


if __name__ == '__main__':
    strategy = Stratego()
    app = QApplication(sys.argv)
    ex = StrategoWindow(strategy)
    sys.exit(app.exec_())
