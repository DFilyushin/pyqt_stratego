# -*- coding: utf-8 -*-
import urllib
import json
import requests


def move():
    move_data = {}
    move_data['game_id'] = 100
    move_data['client_id'] = 101
    move_data['x1'] = 1
    move_data['y1'] = 2
    move_data['x2'] = 3
    move_data['y2'] = 4
    response = requests.post('http://127.0.0.1:5000/move', json=move_data)
    data = json.loads(response.text())
    print data


def create_games():
    for i in range(10):
        name_game = "New game " + str(i)
        response = urllib.urlopen('http://127.0.0.1:5000/new/blue?name=%s' % name_game)
        data = json.loads(response.read())
        print(data)


if __name__ == '__main__':
    move()

