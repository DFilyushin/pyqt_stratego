import sys
from Stratego import Stratego, Coord, Figure, EmptyField, Lake, Place
from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QIcon, QAction
from PyQt4.QtGui import QMainWindow, QApplication, QWidget, QGridLayout, QPixmap, QPushButton, QGraphicsGridLayout, QLabel, QPicture


class FigureLabel(QLabel):
    def __init__(self, *args, **kwargs):
        QLabel.__init__(self, *args, **kwargs)
        self._data = None
        self._selected = False

    def data(self):
        return self._data

    def setData(self, data):
        self._data = data

    def selected(self):
        return self._selected

    def setSelected(self, value):
        if value:
            self.setStyleSheet("border:1px solid blue; border-radius: 5px;background-color: transparent;")
        else:
            self.setStyleSheet('')
        self._selected = value
