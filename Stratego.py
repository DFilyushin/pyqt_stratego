# -*- coding: utf-8 -*-
import random
import copy
import datetime


class Coord:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __ne__(self, other):
        return not self.__eq__(other)


class Figure(object):
    COLOR_BLUE = 'blue'
    COLOR_RED = 'red'

    def __init__(self, name, rank, color, mobile=True):
        self.name = name
        self.rank = rank
        self.color = color
        self.dead = False
        self.opened = False
        self.mobile = mobile

    def __bool__(self):
        return not self.dead

    def __repr__(self):
        return self.name


class Place(object):
    def __init__(self, name, is_blocked=False):
        self.name = name
        self.blocked = is_blocked

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name


class Lake (Place):
    def __init__(self):
        super(Lake, self).__init__('Lak', True)


class EmptyField(Place):
    def __init__(self):
        super(EmptyField, self).__init__('   ')


class FigureSet:
    def __init__(self):
        self.figures = []
        self.load_figures()

    def load_figures(self):
        del self.figures[:]
        self.figures.append(Figure('Flg', 0, Figure.COLOR_BLUE, False))
        self.figures.append(Figure('Flg', 0, Figure.COLOR_RED, False))
        for i in range(6):
            self.figures.append(Figure('Bmb', 11, Figure.COLOR_BLUE, False))
            self.figures.append(Figure('Bmb', 11, Figure.COLOR_RED, False))
        for i in range(8):
            self.figures.append(Figure('Sol', 2, Figure.COLOR_BLUE))
            self.figures.append(Figure('Sol', 2, Figure.COLOR_RED))
        for i in range(5):
            self.figures.append(Figure('Min', 3, Figure.COLOR_BLUE))
            self.figures.append(Figure('Min', 3, Figure.COLOR_RED))
        for i in range(4):
            self.figures.append(Figure('Srg', 4, Figure.COLOR_BLUE))
            self.figures.append(Figure('Srg', 4, Figure.COLOR_RED))
            self.figures.append(Figure('Ltn', 5, Figure.COLOR_BLUE))
            self.figures.append(Figure('Ltn', 5, Figure.COLOR_RED))
            self.figures.append(Figure('Cpt', 6, Figure.COLOR_BLUE))
            self.figures.append(Figure('Cpt', 6, Figure.COLOR_RED))
        for i in range(3):
            self.figures.append(Figure('Mjr', 7, Figure.COLOR_BLUE))
            self.figures.append(Figure('Mjr', 7, Figure.COLOR_RED))
        for i in range(2):
            self.figures.append(Figure('Col', 8, Figure.COLOR_BLUE))
            self.figures.append(Figure('Col', 8, Figure.COLOR_RED))
        self.figures.append(Figure('Gnr', 9, Figure.COLOR_BLUE))
        self.figures.append(Figure('Gnr', 9, Figure.COLOR_RED))
        self.figures.append(Figure('Mar', 10, Figure.COLOR_BLUE))
        self.figures.append(Figure('Mar', 10, Figure.COLOR_RED))
        self.figures.append(Figure('Spy', 1, Figure.COLOR_BLUE))
        self.figures.append(Figure('Spy', 1, Figure.COLOR_RED))

    def get_blue_set(self):
        return [figure for figure in self.figures if figure.color == Figure.COLOR_BLUE]

    def get_red_set(self):
        return [figure for figure in self.figures if figure.color == Figure.COLOR_RED]


class WarField:
    def __init__(self):
        self.w, self.h = 10, 10
        lake = Lake()
        hill = EmptyField()
        self.field = [[hill for x in range(self.w)] for y in range(self.h)]

        self.field[4][2] = lake
        self.field[4][3] = lake
        self.field[5][2] = lake
        self.field[5][3] = lake
        self.field[4][6] = lake
        self.field[4][7] = lake
        self.field[5][6] = lake
        self.field[5][7] = lake

    def set_field(self, figure_set, color):
        i = 0
        row = 0 if color == Figure.COLOR_BLUE else 6
        for x in range(4):
            for y in range(10):
                self.field[x+row][y] = figure_set[i]
                i += 1

    def get_field_as_json(self, color):
        json = []
        for x in reversed(range(self.w)):
            for y in range(self.h):
                item = {}
                item['x'] = x
                item['y'] = y
                if isinstance(self.field[x][y], Figure):
                    figure = self.field[x][y]
                    if figure.color == color:
                        item['figure'] = self.field[x][y].rank
                    else:
                        item['figure'] = '?'
                else:
                    item['figure'] = self.field[x][y].name
                json.append(item)
        return json

    def print_field(self):
        for x in reversed(range(self.w)):
            line = str(x)
            for y in range(self.h):
                if self.field[x][y]:
                    position = self.field[x][y].name
                else:
                    position = '***'
                line = line + position
            print(line)

    def print_field_gamer(self, gamer):
        for x in reversed(range(self.w)):
            line = str(x)
            for y in range(self.h):
                if self.field[x][y]:
                    if isinstance(self.field[x][y], Place):
                        position = str(self.field[x][y])
                    elif isinstance(self.field[x][y], Figure):
                        if gamer == self.field[x][y].color:
                            position = self.field[x][y].name
                        else:
                            position = '???'
                else:
                    position = '***'
                line = line + position
            print(line)

    def set_position(self, c1, field_object):
        self.field[c1.x][c1.y] = field_object

    def get_position(self, c1):
        return self.field[c1.x][c1.y]


class Stratego:

    def __init__(self, game_id='', game_name=''):
        self._name = game_name
        self.game_over = False
        self.winner = ''
        self.who_next = self.who_the_first()
        figures = FigureSet()
        self.blueGamer = figures.get_blue_set()
        self.redGamer = figures.get_red_set()
        self.field = WarField()
        self.redClient = None
        self.blueClient = None
        self.game_id = game_id
        self.started = datetime.datetime.now()
        self.ended = None
        self._message = None

    def end_game(self):
        self.ended = datetime.datetime.now()
        self.game_over = True
        self.winner = 'undefined'

    def name(self):
        return self._name

    def message(self):
        return self._message

    def is_full(self):
        return bool(self.redClient) and bool(self.blueClient)

    def get_client_field(self, client_id):
        color = self.get_color_client(client_id)
        return self.field.get_field_as_json(color)

    def get_color_client(self, client_id):
        if self.redClient == client_id:
            return Figure.COLOR_RED
        elif self.blueClient == client_id:
            return Figure.COLOR_BLUE

    def set_client(self, color, client_id):
        if color == Figure.COLOR_BLUE:
            self.blueClient = client_id
        elif color == Figure.COLOR_RED:
            self.redClient = client_id

    def get_free_client(self):
        if not self.redClient:
            return Figure.COLOR_RED
        if not self.blueClient:
            return Figure.COLOR_BLUE

    def new_game(self):
        random.shuffle(self.blueGamer)
        random.shuffle(self.redGamer)
        self.field.set_field(self.blueGamer, Figure.COLOR_BLUE)
        self.field.set_field(self.redGamer, Figure.COLOR_RED)

    def get_field(self):
        return self.field

    def who_the_first(self):
        return random.choice([Figure.COLOR_BLUE, Figure.COLOR_RED])

    def attack(self, current, target):
        attacker = self.field.get_position(current)
        enemy = self.field.get_position(target)

        # Attacker == enemy
        if enemy.color == attacker.color:
            self._message = 'Destination is our'
            print('Destination is our')
            return False

        # Attackers is winner
        if attacker.rank > enemy.rank:
            attacker.opened = True
            enemy.dead = True
            self.field.set_position(target, attacker)
            self.field.set_position(current, EmptyField())
            if enemy.rank == 0:
                self.game_over = True
                self.winner = attacker.color
                self.ended = datetime.datetime.now()
            self._message = "%s %s has kill %s %s" % (attacker.name, attacker.color,  enemy.name, enemy.color)
        # attackers and enemy is equal
        elif attacker.rank == enemy.rank:
            attacker.dead = True
            enemy.dead = True
            self.field.set_position(current, EmptyField())
            self.field.set_position(target, EmptyField())
            self._message = "%s %s and %s %s has been killed" % (attacker.name, attacker.color, enemy.name, enemy.color)
        else:
            # spy vs Marschal
            if attacker.rank == 1 and enemy.rank == 10:
                enemy.dead = True
                attacker.opened = True
                self.field.set_position(target, attacker)
                self.field.set_position(current, EmptyField())
                self._message = "Spy %s kill Marshal %s" % (attacker.color, enemy.color)
            # Miner vs Bomb
            elif attacker.rank == 3 and enemy.rank == 11:
                enemy.dead = True
                attacker.opened = True
                self.field.set_position(target, attacker)
                self.field.set_position(current, EmptyField())
                self._message = "Miner has been defused bomb"
            else:
                attacker.dead = True
                enemy.opened = True
                self.field.set_position(current, EmptyField())
                self._message = "Enemy %s kill attacker %s" % (enemy.name, attacker.name)

        print(self._message)
        self.who_next = Figure.COLOR_BLUE if attacker.color == Figure.COLOR_RED else Figure.COLOR_RED
        return True

    def move(self, coord_cur, coord_new):
        # diagonal move blocked
        if coord_cur.x != coord_new.x and coord_cur.y != coord_new.y:
            self._message = 'Movement is invalid'
            print('Movement is invalid')
            return False

        # borders
        if coord_new.x < 0 or coord_new.x > 9 or coord_new.y < 0 or coord_new.y > 9:
            self._message = 'Movement is invalid'
            print('Movement is invalid')
            return False

        # figures don't fly
        inc_x = cmp(coord_new.x, coord_cur.x)
        inc_y = cmp(coord_new.y, coord_cur.y)

        tmp = copy.copy(coord_cur)
        is_fly = False
        while True:
            tmp.x += inc_x
            tmp.y += inc_y
            if not isinstance(self.field.get_position(tmp), EmptyField):
                if coord_new != tmp:
                    is_fly = True
                    break
            if coord_new == tmp:
                break

        if is_fly:
            self._message = 'Figure isn''t fly'
            print('Figure isn''t fly')
            return False

        if isinstance(self.field.get_position(coord_cur), Place):
            self._message = 'This not figure'
            print('This not figure')
            return False

        figure = self.field.get_position(coord_cur)
        if figure.color != self.who_next:
            self._message = 'Now step is %s' % self.who_next
            print(self._message)
            return False

        if not figure.mobile:
            self._message = 'Figure is static'
            print(self._message)
            return False

        # destination is blocked (Lake?)
        if isinstance(self.field.get_position(coord_new), Figure):
            return self.attack(coord_cur, coord_new)
        else:
            # target position is Place
            if self.field.get_position(coord_new).blocked:
                self._message = 'Destination place is blocked'
                print(self._message)
                return False

        if (abs(coord_cur.x - coord_new.x) > 1 or abs(coord_cur.y - coord_new.y) > 1) \
                and figure.rank != 2:
                    self._message = 'Figure step one field!'
                    print(self._message)
                    return False

        # simple move
        self.field.set_position(coord_new, figure)
        self.field.set_position(coord_cur, EmptyField())

        self.who_next = Figure.COLOR_BLUE if figure.color == Figure.COLOR_RED else Figure.COLOR_RED
        return True


if __name__ == '__main__':
    game = Stratego()
    print("Game ready")
    print('%s is first gamer' % game.who_next)

    print("-" * 10)

    while not game.game_over:
        command = raw_input('Enter command:')
        line_command = command.lower().split()

        if len(command) == 0:
            continue
        command = line_command[0]
        if command == 'exit':
            break
        elif command == 'go':
            coord1 = line_command[1].split(',')
            coord2 = line_command[2].split(',')

            old_coord = Coord(int(coord1[0]), int(coord1[1]))
            new_coord = Coord(int(coord2[0]), int(coord2[1]))
            game.move(old_coord, new_coord)
            print('%s is next gamer' % game.who_next)

        elif command == 'show':
            if len(line_command) > 1:
                game.field.print_field_gamer(line_command[1])
            else:
                game.field.print_field()
            print('%s is next gamer' % game.who_next)

    print('Game winner %s' % game.winner)