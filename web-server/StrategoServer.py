import uuid
from flask import Flask, jsonify, abort, make_response, request
from Stratego import Stratego, Coord
from flask import request

app = Flask(__name__)
games = {}


@app.route('/game/<string:game_id>/<string:client_id>', methods=['GET'])
def get_field(game_id, client_id):
    if game_id not in games:
        abort(404)
    game = games[game_id]
    return jsonify({'field': game.get_client_field(client_id)})


@app.route('/move', methods=['POST'])
def put_move():
    if not request.json:
        abort(404)
    x1 = request.json['x1']
    y1 = request.json['y1']
    x2 = request.json['x2']
    y2 = request.json['y2']
    client_id = request.json['client_id']
    game_id = request.json['game_id']
    game = games[game_id]
    result = game.move(Coord(x1, y1), Coord(x2, y2))
    if result:
        status = 'Ok'
        message = ''
    else:
        status = 'No'
        message = game.message()
    return jsonify(
        {
            'message': message,
            'status': status
        }), 201


@app.route('/new/<string:color>', methods=['GET'])
def get_new_game(color):
    name_game = request.args.get('name')
    game_id = str(uuid.uuid4())
    game = Stratego(game_id, name_game)
    game.new_game()
    client_id = str(uuid.uuid4())
    game.set_client(color, client_id)
    games[game_id] = game
    print "new game %s, %s, %s" % (game_id, client_id, color)
    return jsonify(
        {
            'game_id': game_id,
            'game_name': name_game,
            'color': color,
            'client_id': client_id,
            'status': 'Ok'
        }), 201


@app.route('/games', methods=['GET'])
def get_games():
    g = [{'id': item, 'name': games[item].name(), 'is_full': games[item].is_full(), 'free': games[item].get_free_client()} for item in games.keys()]
    return jsonify({'games': g})


@app.route('/games/count', methods=['GET'])
def get_count_game():
    return jsonify({'count': len(games), 'status': 'Ok'}), 201


@app.route('/game/<string:game_id>/', methods=['GET'])
def get_game(game_id):
    if game_id not in games:
        abort(404)
    game = games[game_id]
    return jsonify(
        {
            'game_id': game_id,
            'is_full': game.is_full(),
            'started': str(game.started),
            'ended': str(game.ended),
            'winner': game.winner,
            'who_next': game.who_next,
            'status': 'Ok'
        }), 201


@app.route('/connect/<string:game_id>', methods=['GET'])
def connect_to_game(game_id):
    if game_id not in games:
        abort(404)
    game = games[game_id]
    if game.is_full():
        return jsonify({'game_id': game_id, 'color': '', 'status': 'Server full'}), 201
    free_color = game.get_free_client()
    client_id = str(uuid.uuid4())
    game.set_client(free_color, client_id)
    return jsonify(
        {
            'game_id': game_id,
            'color': free_color,
            'client_id': client_id,
            'status': 'Ok'
        }), 201


@app.route('/color/<string:game_id>/<string:client_id>', methods=['GET'])
def get_color_client(game_id, client_id):
    current_game = games[game_id]
    if not current_game:
        abort(404)
    return jsonify(
        {'game_id': game_id,
         'client': client_id,
         'color': current_game.get_color_client(client_id),
         'status': 'Ok'
         }), 201


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'status': 'error', 'message': 'Not found'}), 404)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')



