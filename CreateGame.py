# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore

class CreateGameDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(CreateGameDialog, self).__init__(parent)

        self.selected = None
        self.buttonBox = QtGui.QDialogButtonBox(self)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)

        self.titleServer = QtGui.QLabel(self)
        self.titleServer.setText('Server name')
        self.edtServer = QtGui.QLineEdit(self)
        self.edtServer.setText('http://127.0.0.1:5000')

        self.titleName = QtGui.QLabel(self)
        self.titleName.setText('Name')
        self.edtGame = QtGui.QLineEdit(self)

        self.colorGamer = QtGui.QLabel(self)
        self.colorGamer.setText('Select color gamer')

        self.radio1 = QtGui.QRadioButton("Blue")
        self.radio2 = QtGui.QRadioButton("Red")
        self.radio1.setChecked(True)

        self.verticalLayout = QtGui.QVBoxLayout(self)
        self.verticalLayout.addWidget(self.titleServer)
        self.verticalLayout.addWidget(self.edtServer)

        self.verticalLayout.addWidget(self.titleName)
        self.verticalLayout.addWidget(self.edtGame)

        self.verticalLayout.addWidget(self.colorGamer)
        self.verticalLayout.addWidget(self.radio1)
        self.verticalLayout.addWidget(self.radio2)

        self.verticalLayout.addWidget(self.buttonBox)

        self.edtGame.setFocus()

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    def get_game_name(self):
        return self.edtGame.text()

    def get_gamer_color(self):
        if self.radio1.isChecked():
            return 'blue'
        if self.radio2.isChecked():
            return 'red'
    def get_server(self):
        return self.edtServer.text()

    @staticmethod
    def create_game(parent=None):
        dialog = CreateGameDialog(parent)
        result = dialog.exec_()
        server = dialog.get_server()
        name = dialog.get_game_name()
        color = dialog.get_gamer_color()
        return server, name, color, result == QtGui.QDialog.Accepted
