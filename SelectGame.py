# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore
import urllib
import json


class SelectGameDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(SelectGameDialog, self).__init__(parent)

        self.selected = None
        self.buttonBox = QtGui.QDialogButtonBox(self)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)

        self.titleServer = QtGui.QLabel(self)
        self.titleServer.setText('Select server')

        self.edtServer = QtGui.QLineEdit(self)
        self.btnGetGame = QtGui.QPushButton(self)
        self.btnGetGame.setText('Refresh')
        self.btnGetGame.clicked.connect(self.get_games)

        self.titleGame = QtGui.QLabel(self)
        self.titleGame.setText('List games')

        self.listGame = QtGui.QListWidget(self)
        self.listModel = QtGui.QStandardItemModel(self.listGame)

        self.edtServer.setText('http://127.0.0.1:5000')

        self.verticalLayout = QtGui.QVBoxLayout(self)
        self.verticalLayout.addWidget(self.titleServer)
        self.verticalLayout.addWidget(self.edtServer)
        self.verticalLayout.addWidget(self.btnGetGame)
        self.verticalLayout.addWidget(self.titleGame)
        self.verticalLayout.addWidget(self.listGame)
        self.verticalLayout.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    def get_games(self):
        server = self.edtServer.text()
        s = '%s/games' % server
        response = urllib.urlopen(s)
        data = json.loads(response.read())
        self.listModel.clear()
        for game in data['games']:
            if game['is_full']:
                continue
            item = QtGui.QListWidgetItem()
            item.setText(game['name'])
            item.setData(QtCore.Qt.UserRole, game['id'])
            self.listGame.addItem(item)

    def get_selected(self):
        if self.listGame.currentItem():
            item = self.listGame.currentItem().text()
            item_data = self.listGame.currentItem().data(QtCore.Qt.UserRole).toPyObject()
            return item, item_data
        else:
            return None, None

    def get_server(self):
        return self.edtServer.text()

    @staticmethod
    def get_game(parent=None):
        dialog = SelectGameDialog(parent)
        result = dialog.exec_()
        selected = dialog.get_selected()
        server = dialog.get_server()
        return server, selected[0], selected[1], result == QtGui.QDialog.Accepted
